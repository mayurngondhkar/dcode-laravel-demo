<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'TasksController@index')->name('dashboard');
Route::get('/task/{id}', 'TasksController@show')->name('task');
Route::get('/edit/{id}', 'TasksController@edit')->name('edit');
Route::post('/edit', 'TasksController@update')->name('update');
Route::get('/create', 'TasksController@create')->name('create');
Route::post('/store', 'TasksController@store')->name('store');
Route::get('/delete/{id}', 'TasksController@softDelete')->name('delete');
Route::get('/mytasks', 'TasksController@getMyTasks')->name('mytasks');
