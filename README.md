# dcode-laravel-demo

## Requirements
* Create task system with user login
    * Access a URL
    * Login to a task list
    * Add a task
    * Edit a task
    * Delete a task
* Key Steps
   1. Create a blank repository in BitBucket;
   2. Create a virtual environment for your project;
   3. Add Laravel code to the virtual environment and synchronise to your BitBucket repository;
   4. Create the blank Laravel project;
   5. Use Migrations and Seeds to create tables and pre-populate content; and
   6. Finalise your project and synchronise with BitBucket.

## Dev Info
> Laravel Framework 5.4.36

#### Running The Project
* Create .env file and set database
* In the .env file set "APP_NAME" to "Task Manager"


* Run "composer install"


* Run migrations
* Run seeder
* > Testing Login after Seeding

Email: user@test.com

Password: 123456


* Look at the seeding files for more info.

## Useage
* After logging in the user is taken to the "Dashboard"
##### Viewing Tasks
* In the dashboard a user can view all tasks
* Click the "My Tasks" button in the sidebar to view tasks that have been assigned to the logged in user.
##### Adding and Editing a task
* To add a new task click the "Add Task" button either in the dashboard page, my tasks page or the sidebar.
* To create a task click on he "Add Task" button located on the "Dashboard" or "My Tasks" page.
* The user who has created the task or a user to who the task is assigned to may edit a task. The task may be edited after clicking the "Edit Task" button.
* The user may assign the task to a different user.
* The user may set the task status to "Active" or "Completed"
##### Deleting a task
* Only a user who has created a task can delete it. This can be done in the task page after clicking the "View Task" button present in the "My Tasks" page or the "Dashboard".

* NOTE: Only Authorised users may VIEW the "Delete Task" or the "Edit Task" buttons which is present in the "View Task" page.
