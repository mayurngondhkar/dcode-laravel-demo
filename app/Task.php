<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'description', 'status', 'assigned_to_user_id', 'user_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function assignedUser() {
        return $this->belongsTo('App\User');
    }
}
