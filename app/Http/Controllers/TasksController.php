<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Task;
use Auth;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $user = auth()->user();
        $userName = $user['name'];

        $tasksData = (new \App\Task)->join(
            'users',
            'tasks.user_id',
            '=',
            'users.id'
        )->join(
            'users as users1',
            'tasks.assigned_to_user_id',
            '=',
            'users1.id'
        )->select(
            'tasks.id',
            'tasks.name',
            'tasks.status',
            'user_id',
            'assigned_to_user_id',
            'users.name as created_by_user',
            'users1.name as assigned_to_user'
        )->paginate(5);

        return view('task.dashboard', [
            'userName' => $userName,
            'tasksData' => $tasksData
        ]);
    }

    public function getMyTasks()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $user = auth()->user();
        $userName = $user['name'];

        $tasksData = (new \App\Task)->join(
            'users',
            'tasks.user_id',
            '=',
            'users.id'
        )->join(
            'users as users1',
            'tasks.assigned_to_user_id',
            '=',
            'users1.id'
        )->select(
            'tasks.id',
            'tasks.name',
            'tasks.status',
            'user_id',
            'assigned_to_user_id',
            'users.name as created_by_user',
            'users1.name as assigned_to_user'
        )->where('assigned_to_user_id', '=', $user['id'])->paginate(5);

        return view('task.tasks', [
            'userName' => $userName,
            'tasksData' => $tasksData
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $users = User::select('id', 'name')->get();
        return view('task.createTask', ['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $this->validate($request, [
            'name' => 'required|min:5|max:25',
            'description' => 'required|min:10|max:255',
            'status' => 'required',
            'assigned_to' => 'required'
        ]);

        $user = Auth::user();

        $task = new Task([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'assigned_to_user_id' => $request->input('assigned_to'),
            'user_id' => $user['id']
        ]);

        $user->tasks()->save($task);

        return redirect()->route('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $user = auth()->user();

        $task = $this->getTask($id);
        $isOwner = $task['user_id'] === $user['id'] ? true : false;
        $isAssigned = $task['assigned_to_user_id'] === $user['id'] ? true : false;

        return view('task.task', ['task' => $task, 'isOwner' => $isOwner, 'isAssigned' => $isAssigned]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $users = User::select('id', 'name')->get();

        $task = $this->getTask($id);

        return view('task.editTask', ['task' => $task, 'users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $task = Task::find($request->input('id'));
        $task['name'] = $request->input('name');
        $task['description'] = $request->input('description');
        $task['status'] = $request->input('status');
        $task['assigned_to_user_id'] = $request->input('assigned_to');
        $task->save();

        return redirect()->route('task', $task['id'])->with('info', 'Updated ' . $request->input('name') . ' task');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function softDelete ($id) {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $task = Task::find($id);
        $task->delete();

        return redirect()->route('dashboard');
    }

    private function getTask($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $task = Task::where('id', $id)->first();
        $assigned_to_user = User::where('id', $task['assigned_to_user_id'])->first();
        $created_by_user = User::where('id', $task['user_id'])->first();
        $task['assigned_to_user'] = $assigned_to_user['name'];
        $task['created_by_user'] = $created_by_user['name'];
        $task['created_date'] = date('F j, Y, g:i a', strtotime($task['created_at']));

        return $task;
    }
}
