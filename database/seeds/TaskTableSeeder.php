<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = new \App\Task([
            "name" => "Task 1",
            "description" => "This is task 1",
            "status" => true,
            "user_id" => "1",
            "assigned_to_user_id" => "1"
        ]);
        $task->save();
        $task = new \App\Task([
            "name" => "Task 2",
            "description" => "This is task 2",
            "status" => true,
            "user_id" => "1",
            "assigned_to_user_id" => "2"
        ]);
        $task->save();
        $task = new \App\Task([
            "name" => "Task 3",
            "description" => "This is task 3",
            "status" => true,
            "user_id" => "3",
            "assigned_to_user_id" => "2"
        ]);
        $task->save();
        $task = new \App\Task([
            "name" => "Task 4",
            "description" => "This is task 4",
            "status" => true,
            "user_id" => "4",
            "assigned_to_user_id" => "1"
        ]);
        $task->save();
    }
}
