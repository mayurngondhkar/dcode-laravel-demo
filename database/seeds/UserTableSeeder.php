<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        static $password;

        $user = new \App\User([
            "name" => "User",
            "email" => "user@test.com",
            'password' => $password ?: $password = bcrypt('123456'),
            'remember_token' => str_random(10),
        ]);
        $user->save();

        factory(App\User::class, 10)->create()->each(function ($u) {
            $u->tasks()->save(factory(App\Task::class)->make());
        });
    }
}
