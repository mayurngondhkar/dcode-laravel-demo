<link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
<div class="wrapper">
    <!-- Sidebar -->
    <nav class="">
        <div class="">
            <h3>Task Manager</h3>
        </div>

        <ul class="list-unstyled">
            <li>
                <a href="{{ route('dashboard') }}" class="btn btn-info sidebar-btns">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('mytasks') }}" class="btn btn-info sidebar-btns">My Tasks</a>
            </li>
            <li>
                <a href="{{ route('create') }}" class="btn btn-info sidebar-btns">Add Task</a>
            </li>
        </ul>
    </nav>
</div>