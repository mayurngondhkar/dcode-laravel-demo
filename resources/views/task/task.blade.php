@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/task.css') }}" rel="stylesheet">
    <div class="container">
        <div class="panel">
            <div class="panel-heading">
                <span class="text-uppercase">Task: {{ $task['name'] }}</span>
                <div class="task-btns">
                    @if($isAssigned || $isOwner)
                    <a class="btn btn-primary" href="{{ route('edit', ['id' => $task['id']]) }}">Edit Task</a>
                    @endif
                    @if($isOwner)
                        <a class="btn btn-danger" href="{{ route('delete', ['id' => $task['id']]) }}">Delete Task</a>
                    @endif
                </div>
            </div>
            <div class="panel-body">
                <div class="task-grid">
                    <label class="task-grid-label">Task Name</label>&nbsp;
                    <span class="task-grid-value">{{ $task['name'] }}</span>
                    <label class="task-grid-label">Task Description</label>&nbsp;
                    <span class="task-grid-value">{{ $task['description'] }}</span>
                    <label class="task-grid-label">Task Status</label>&nbsp;
                    @if($task['status'] === 1)
                        <span class="task-grid-value">Active</span>
                    @elseif($task['status'] === 0)
                        <span class="task-grid-value">Completed</span>
                    @endif
                    <label class="task-grid-label">Created By</label>&nbsp;
                    <span class="task-grid-value">{{ $task['created_by_user'] }}</span>
                    <label class="task-grid-label">Assigned To</label>&nbsp;
                    <span class="task-grid-value">{{ $task['assigned_to_user'] }}</span>
                    <label class="task-grid-label">Created On</label>&nbsp;
                    <span class="task-grid-value">{{ $task['created_date'] }}</span>
                </div>
            </div>
        </div>
    </div>
@endsection
