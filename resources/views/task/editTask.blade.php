@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/editTask.css') }}" rel="stylesheet">
    <div class="container">
        <div class="panel">
            <div class="panel-heading">
                <span class="text-uppercase">Edit: {{ $task['name'] }}</span>
                <div class="task-btns">
                    <button class="btn btn-primary ">Cancel Edit</button>
                    <button class="btn btn-danger ">Delete Task</button>
                </div>
            </div>
            <div class="panel-body">
                <form action="{{ route('update') }}" method="post">
                    <div class="form-group">
                        <label>Task Name</label>
                        <input name="name" type="text" class="form-control" value="{{ $task['name'] }}" required>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input name="description" type="text" class="form-control" value="{{ $task['description'] }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            @if($task['status'] === 0)
                                <option value="1">Active</option>
                                <option value="0" selected>Completed</option>
                            @elseif($task['status'] === 1)
                                <option value="1" selected>Active</option>
                                <option value="0">Completed</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Assign to</label>
                        <select name="assigned_to" class="form-control">
                            @foreach($users as $user)
                                <option value="{{ $user['id'] }}">{{ $user['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="id" value="{{ $task['id'] }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
