@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="dashboard-panel">
                <div class="panel">
                    <div class="panel-title">{{ $userName }}'s Dashboard</div>
                    <div class="panel-heading">
                        <a class="btn btn-primary add-task" href="{{ route('create') }}">Add Task</a>
                    </div>
                    <div class="panel-heading">
                        All Tasks
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <td>Task Name</td>
                                <td>Status</td>
                                <td>Created By</td>
                                <td>Assigned To</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tasksData as $task)
                                <tr>
                                    <td>{{ $task['name'] }}</td>
                                    @if($task['status'] === 1)
                                        <td>Active</td>
                                    @elseif($task['status'] === 0)
                                        <td>Completed</td>
                                    @endif
                                    <td>{{ $task['created_by_user'] }}</td>
                                    <td>{{ $task['assigned_to_user'] }}</td>
                                    <td><a class="btn btn-info" href="{{ route('task', ['id' => $task->id]) }}">View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div>{{ $tasksData->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
