@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/editTask.css') }}" rel="stylesheet">
    <div class="container">
        <div class="panel">
            <div class="panel-heading">
                <span class="text-uppercase">Create A New Task</span>

            </div>
            <div class="panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('store') }}" method="post">
                    <div class="form-group">
                        <label>Task Name</label>
                        <input name="name" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input name="description" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                                <option value="1" selected>Active</option>
                                <option value="0">Completed</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Assign to</label>
                        <select name="assigned_to" class="form-control">
                            @foreach($users as $user)
                                <option value="{{ $user['id'] }}">{{ $user['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-primary" href="{{ route('dashboard') }}">Cancel Create</a>
                </form>
            </div>
        </div>
    </div>
@endsection